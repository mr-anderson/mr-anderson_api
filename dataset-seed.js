// noinspection JSUnresolvedVariable
module.exports = () => {
    const data = {};

    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    data.agent = [];
    data.ticket = [];
    let ticket_id = 0;
    for (let i = 0; i < 1000; i++) {
        data.agent.push({id: i, name: `agent_${i}`});

        for (let j = 0; j < getRandomInt(50); j++) {
            ticket_id++;
            data.ticket.push({id: ticket_id, agent_id: i, subject: `ticket_${ticket_id}`});
        }
    }

    return data
};
